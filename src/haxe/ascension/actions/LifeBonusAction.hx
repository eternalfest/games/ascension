package ascension.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class LifeBonusAction implements IAction {
    public var name(default, null): String = Obfu.raw("lifeBonus");
    public var isVerbose(default, null): Bool = false;

    private var mod: Ascension;

    public function new(mod: Ascension) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();

        this.mod.lifeBonus(game);

        return false;
    }
}
