package ascension.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class AddLifeAction implements IAction {
    public var name(default, null): String = Obfu.raw("addLife");
    public var isVerbose(default, null): Bool = false;

    private var mod: Ascension;

    public function new(mod: Ascension) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();

        this.mod.addLife(game);

        return false;
    }
}
