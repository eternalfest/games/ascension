package ascension;
import ascension.actions.AddLifeAction;
import ascension.actions.LifeBonusAction;

import merlin.IAction;

@:build(patchman.Build.di())
class Ascension {

    @:diExport
    public var addLifeAction(default, null): IAction;
    @:diExport
    public var lifeBonusAction(default, null): IAction;

    public var id: Int;

    public function new() {
        this.addLifeAction = new AddLifeAction(this);
        this.lifeBonusAction = new LifeBonusAction(this);
        this.id = 0;
    }

    public function addLife(game: hf.mode.GameMode) {
        var lives_nbr: Int = 1;
        var players: Array<hf.entity.Player> = game.getPlayerList();
        var players_lives: Int = Math.floor(lives_nbr / players.length);
        var addition: Int = lives_nbr - players_lives * players.length;
        if(addition > 0) {
            var rand = players[this.id];
            rand.lives += addition;
            game.gi.setLives(this.id, players[this.id].lives);
            this.id = (this.id + 1) % players.length;
        }

        if (players_lives != 0) {
            for (i in 0...(players.length)) {
                players[i].lives += players_lives;
                if (players[i].lives < 0)
                    players[i].lives = 0;
                game.gi.setLives(i, players[i].lives);
            }
            game.fxMan.attachAlert("+1 VIE !");
        }
    }

    public function lifeBonus(game: hf.mode.GameMode) {
        var players: Array<hf.entity.Player> = game.getPlayerList();
        for (i in 0...players.length) {
            var bonus = players[i].lives * 50000;
            players[i].getScore(players[i], bonus);
            game.fxMan.attachAlert(game.root.Lang.get(35) + players[i].lives + ' x ' + game.root.Data.formatNumber(50000));
        }
    }
}
